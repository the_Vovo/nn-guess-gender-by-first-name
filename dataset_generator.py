import csv
import random
import numpy as np

name_train_array = []
gender_train_array = []
name_test_array = []
gender_test_array = []
def generate_datasets():
    with open('gender_refine-csv.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        data = list(reader)
        for item in data:
            reformatted_name = [ord(letter) / 1000 for letter in item[0]]
            tensor_name = []
            gender = int(item[1])
            for i in range(20):
                if i < len(reformatted_name):
                    tensor_name.append(reformatted_name[i])
                else:
                    tensor_name.append(0)
            if random.random() <= 0.8:
                name_train_array.append(tensor_name)
                gender_train_array.append(gender)
            else:
                name_test_array.append(tensor_name)
                gender_test_array.append(gender)
    return np.array(name_train_array), np.array(gender_train_array), np.array(name_test_array), np.array(gender_test_array)


a = np.array(generate_datasets()[0])
