import tensorflow.keras as keras
import tensorboard
import tensorflow as tf
import numpy as np
import matplotlib as plt
import dataset_generator
from datetime import datetime


full_dataset = dataset_generator.generate_datasets()
name_train_array = full_dataset[0]
gender_train_array = full_dataset[1]
name_test_array = full_dataset[2]
gender_test_array = full_dataset[3]

name_train_array.tofile('train.csv', sep=',', format='%10.5f')
name_test_array.tofile('test.csv', sep=',', format='%10.5f')
# gender_train_array.tofile('gender_train.csv', sep=',', format='%10.5f')
gender_test_array.tofile('gender_test.csv', sep=',', format='%10.5f')

# Define the Keras TensorBoard callback.
# logdir="logs/fit/"
# print(logdir)
# tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)


def create_and_test_model():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Flatten(input_shape=(20,)))
    model.add(tf.keras.layers.Dense(1460, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(730, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(730, activation=tf.nn.relu))
    model.add(tf.keras.layers.Dense(4, activation=tf.nn.softmax))

# Optimizing model
    model.compile(optimizer='adam',
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])
    model.fit(name_train_array, gender_train_array, epochs=4)

    model.save('name-gender.model')

    val_loss, val_acc = model.evaluate(name_test_array, gender_test_array)
    print(val_loss)
    print(val_acc)


create_and_test_model()

# Predictions
new_model = tf.keras.models.load_model('name-gender.model')

for i in range(66):
    predictions = new_model.predict(name_test_array)
    print(predictions)
#
    print(np.argmax(predictions[i]))
