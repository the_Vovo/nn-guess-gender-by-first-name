import dataset_generator

train_dataset = dataset_generator.generate_datasets()[0]
test_dataset = dataset_generator.generate_datasets()[2]

primer = []


def assess_dataset(dataset):
    for name in dataset:
        for letter in name:
            if letter in primer:
                pass
            else:
                primer.append(letter)


assess_dataset(train_dataset)
assess_dataset(test_dataset)
print(len(primer))